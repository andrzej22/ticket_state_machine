<?php

namespace App\Workflow\State;

class TicketState
{
    public const REGISTERED = 'registered';
    public const PAID = 'paid';
    public const CANCELED = 'canceled';

}