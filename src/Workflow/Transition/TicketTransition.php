<?php

namespace App\Workflow\Transition;

class TicketTransition
{
    public const TO_PAID = 'to_paid';
    public const TO_CANCELED = 'to_canceled';

}