<?php

namespace App\Workflow;

use App\Entity\Ticket;
use App\Exception\NotAllowedTransitionException;
use App\Exception\TicketNotFoundException;
use App\Exception\ValidatorException;
use App\Repository\TicketRepository;
use App\Workflow\State\TicketState;
use App\Workflow\Transition\TicketTransition;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Workflow\WorkflowInterface;

class TicketWorkflow
{
    public function __construct(private readonly WorkflowInterface $ticketStateMachine, private readonly TicketRepository $ticketRepository, private readonly ValidatorInterface $validator)
    {
    }

    /**
     * @param Ticket $ticket
     * @return Ticket
     * @throws ValidatorException
     */
    public function create(Ticket $ticket): Ticket
    {
        $ticket->setState(TicketState::REGISTERED);
        $validationResult = $this->validator->validate($ticket);

        foreach ($validationResult as $error) {
            throw new ValidatorException($error->getMessage());
        }

        $this->ticketRepository->save($ticket);

        return $ticket;
    }

    /**
     * @param int $ticketId
     * @return Ticket
     * @throws NotAllowedTransitionException
     * @throws TicketNotFoundException
     */
    public function markTicketAsPaid(int $ticketId): Ticket
    {
        $ticket = $this->getTicket($ticketId);

        if ($this->ticketStateMachine->can($ticket, TicketTransition::TO_PAID)) {
            $this->ticketStateMachine->apply($ticket, TicketTransition::TO_PAID);
        } else {
            throw new NotAllowedTransitionException();
        }

        $this->ticketRepository->save($ticket);

        return $ticket;
    }

    /**
     * @param int $ticketId
     * @return Ticket
     * @throws NotAllowedTransitionException
     * @throws TicketNotFoundException
     */
    public function markTicketAsCanceled(int $ticketId): Ticket
    {
        $ticket = $this->getTicket($ticketId);

        if ($this->ticketStateMachine->can($ticket, TicketTransition::TO_CANCELED)) {
            $this->ticketStateMachine->apply($ticket, TicketTransition::TO_CANCELED);
        } else {
            throw new NotAllowedTransitionException();
        }

        $this->ticketRepository->save($ticket);

        return $ticket;
    }

    /**
     * @param int $id
     * @return Ticket
     * @throws TicketNotFoundException
     */
    private function getTicket(int $id): Ticket
    {
        $ticket = $this->ticketRepository->find($id);

        if ($ticket) {
            return $ticket;
        }

        throw new TicketNotFoundException();
    }
}