<?php

namespace App\Service;

use App\Entity\Ticket;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class NotificationService
{
    public function __construct(private MailerInterface $mailer, private LoggerInterface $logger)
    {
    }

    public function sendTicketPaid(Ticket $ticket): void
    {
        $this->logger->info("Ticket {$ticket->getId()} was paid.");

        $email = (new Email())
            ->from('hello@example.com')
            ->to($ticket->getEmail())
            ->subject('Ticket was paid')
            ->text('Your ticker was paid');

        $this->mailer->send($email);
    }

    public function sendTicketCanceled(Ticket $ticket): void
    {
        $this->logger->info("Ticket {$ticket->getId()} was canceled.");

        $email = (new Email())
            ->from('hello@example.com')
            ->to($ticket->getEmail())
            ->subject('Ticket was registered')
            ->text('Your ticker was registered');

        $this->mailer->send($email);
    }
}