<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Exception\NotAllowedTransitionException;
use App\Exception\TicketNotFoundException;
use App\Exception\ValidatorException;
use App\Repository\TicketRepository;
use App\Workflow\TicketWorkflow;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ApiController extends AbstractController
{
    #[Route('/api/tickets', name: 'api_list_tickets', methods: ['GET'])]
    public function tickets(TicketRepository $ticketRepository): JsonResponse
    {
        $tickets = $ticketRepository->findAll();
        return $this->json([
            'success' => true,
            'data' => $tickets,
        ]);
    }

    #[Route('/api/tickets', name: 'api_add_tickets', methods: ['POST'])]
    public function addTicket(Request $request, SerializerInterface $serializer, TicketWorkflow $ticketWorkflow): JsonResponse
    {
        $ticket = $serializer->deserialize($request->getContent(), Ticket::class, 'json');

        try {
            $ticketWorkflow->create($ticket);
        } catch (ValidatorException $exception) {
            return $this->json(
                [
                    'success' => false,
                    'error' => $exception->getMessage()
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->json([
            'success' => true,
            'data' => $ticket
        ]);
    }

    #[Route('/api/tickets/{id}/pay', name: 'api_pay_tickets', methods: ['PUT'])]
    public function payTicket(int $id, TicketWorkflow $ticketWorkflow): JsonResponse
    {
        try {
            $ticket = $ticketWorkflow->markTicketAsPaid($id);
        } catch (NotAllowedTransitionException $exception) {
            return $this->json(
                [
                    'success' => false,
                    'error' => $exception->getMessage()
                ],
                Response::HTTP_FORBIDDEN
            );
        } catch (TicketNotFoundException $exception) {
            return $this->json(
                [
                    'success' => false,
                    'error' => $exception->getMessage()
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->json([
            'success' => true,
            'data' => $ticket
        ]);
    }

    #[Route('/api/tickets/{id}/cancel', name: 'api_cancel_tickets', methods: ['PUT'])]
    public function cancelTicket(int $id, TicketWorkflow $ticketWorkflow): JsonResponse
    {
        try {
            $ticket = $ticketWorkflow->markTicketAsCanceled($id);
        } catch (NotAllowedTransitionException $exception) {
            return $this->json(
                [
                    'success' => false,
                    'error' => $exception->getMessage()
                ],
                Response::HTTP_FORBIDDEN
            );
        } catch (TicketNotFoundException $exception) {
            return $this->json(
                [
                    'success' => false,
                    'error' => $exception->getMessage()
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->json([
            'success' => true,
            'data' => $ticket
        ]);
    }
}
