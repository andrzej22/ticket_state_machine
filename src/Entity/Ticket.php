<?php

namespace App\Entity;

use App\Repository\TicketRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TicketRepository::class)]
class Ticket
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Assert\NotBlank(message: 'The state value should not be blank')]
    #[ORM\Column(length: 255)]
    private ?string $state = null;

    #[Assert\NotBlank(message: 'The departureStation value should not be blank')]
    #[ORM\Column(length: 255)]
    private ?string $departureStation = null;

    #[Assert\NotBlank(message: 'The arrivalStation value should not be blank')]
    #[ORM\Column(length: 255)]
    private ?string $arrivalStation = null;

    #[Assert\NotBlank(message: 'The departureDatetime value should not be blank')]
    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $departureDatetime = null;

    #[Assert\NotBlank(message: 'The arrivalDatetime value should not be blank')]
    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $arrivalDatetime = null;

    #[Assert\NotBlank(message: 'The email value should not be blank')]
    #[Assert\Email]
    #[ORM\Column(length: 255)]
    private ?string $email = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): static
    {
        $this->state = $state;

        return $this;
    }

    public function getDepartureStation(): ?string
    {
        return $this->departureStation;
    }

    public function setDepartureStation(string $departureStation): static
    {
        $this->departureStation = $departureStation;

        return $this;
    }

    public function getArrivalStation(): ?string
    {
        return $this->arrivalStation;
    }

    public function setArrivalStation(string $arrivalStation): static
    {
        $this->arrivalStation = $arrivalStation;

        return $this;
    }

    public function getDepartureDatetime(): ?\DateTimeInterface
    {
        return $this->departureDatetime;
    }

    public function setDepartureDatetime(\DateTimeInterface $departureDatetime): static
    {
        $this->departureDatetime = $departureDatetime;

        return $this;
    }

    public function getArrivalDatetime(): ?\DateTimeInterface
    {
        return $this->arrivalDatetime;
    }

    public function setArrivalDatetime(\DateTimeInterface $arrivalDatetime): static
    {
        $this->arrivalDatetime = $arrivalDatetime;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }
}
