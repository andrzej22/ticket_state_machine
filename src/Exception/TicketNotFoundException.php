<?php

namespace App\Exception;

class TicketNotFoundException extends \Exception
{
    protected $message = 'Ticket not found';
}