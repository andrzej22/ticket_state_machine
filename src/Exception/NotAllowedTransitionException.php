<?php

namespace App\Exception;

class NotAllowedTransitionException extends \Exception
{
    protected $message = 'Not allowed transition.';
}