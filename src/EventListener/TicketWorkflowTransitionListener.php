<?php

namespace App\EventListener;

use App\Entity\Ticket;
use App\Service\NotificationService;
use App\Workflow\Transition\TicketTransition;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Workflow\Event\Event;

#[AsEventListener(event: 'workflow.ticket.transition')]
class TicketWorkflowTransitionListener
{
    public function __construct(private NotificationService $notificationService)
    {
    }

    public function __invoke(Event $event): void
    {
        $transitionName = $event->getTransition()->getName();
        /** @var Ticket $ticket */
        $ticket = $event->getSubject();

        match ($transitionName) {
            TicketTransition::TO_PAID => $this->processPaid($ticket),
            TicketTransition::TO_CANCELED => $this->processCanceled($ticket),
            default => throw new \LogicException("Not enabled transition {$transitionName}")
        };
    }

    private function processPaid(Ticket $ticket): void
    {
        $this->notificationService->sendTicketPaid($ticket);
    }

    private function processCanceled(Ticket $ticket): void
    {
        $this->notificationService->sendTicketCanceled($ticket);
    }

}