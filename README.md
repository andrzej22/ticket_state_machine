Api actions:
 - [GET] `/api/tickets` - list tickets
 - [POST] `/api/tickets` - add new ticket. Json body:
    ```{"email": "test@example.com", "departureStation": "Ternopil", "arrivalStation": "Lviv", "departureDatetime": "10:23 20-03-2024", "arrivalDatetime": "14:53 20-03-2024"}```
 - [PUT] `/api/tickets/{id}/pay` - pay ticket
 - [PUT] `/api/tickets/{id}/cancel` - cancel ticket

