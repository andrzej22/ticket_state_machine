<?php

namespace App\Tests\Workflow;

use App\Entity\Ticket;
use App\Exception\NotAllowedTransitionException;
use App\Exception\ValidatorException;
use App\Workflow\State\TicketState;
use App\Workflow\TicketWorkflow;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TicketWorkflowTest extends KernelTestCase
{

    private TicketWorkflow $ticketWorkflow;

    protected function setUp(): void
    {
        $this->ticketWorkflow = self::getContainer()->get(TicketWorkflow::class);
    }

    public function testCreate(): int
    {
        $ticket = new Ticket();
        $ticket->setState(TicketState::REGISTERED);
        $ticket->setEmail('test@example.com');
        $ticket->setDepartureStation('Ternopil');
        $ticket->setArrivalStation('Kyiv');
        $ticket->setDepartureDatetime((new \DateTime('now'))->modify('+2 hours'));
        $ticket->setArrivalDatetime((new \DateTime('now'))->modify('+8 hours'));


        $ticket = $this->ticketWorkflow->create($ticket);

        $this->assertNotNull($ticket->getId());
        $this->assertSame(TicketState::REGISTERED, $ticket->getState());

        return $ticket->getId();
    }

    /**
     * @depends testCreate
     * @return int
     */
    public function testMarkTicketAsPaid(int $ticketId): int
    {
        $ticket = $this->ticketWorkflow->markTicketAsPaid($ticketId);
        $this->assertNotNull($ticket->getId());
        $this->assertSame(TicketState::PAID, $ticket->getState());

        return $ticket->getId();
    }

    /**
     * @depends testMarkTicketAsPaid
     * @return int
     */
    public function testMarkTicketAsCanceled(int $ticketId): int
    {
        $ticket = $this->ticketWorkflow->markTicketAsCanceled($ticketId);
        $this->assertNotNull($ticket->getId());
        $this->assertSame(TicketState::CANCELED, $ticket->getState());

        return $ticket->getId();
    }

    /**
     * @depends testMarkTicketAsCanceled
     * @return void
     */
    public function testNotAllowedMarkTicketAsPaid(int $ticketId): void
    {
        $this->expectException(NotAllowedTransitionException::class);
        $this->expectExceptionMessage('Not allowed transition.');

        $this->ticketWorkflow->markTicketAsPaid($ticketId);
    }

    public function testInvalidCreation(): void
    {
        $ticket = new Ticket();
        $ticket->setState(TicketState::REGISTERED);
        $ticket->setEmail('testexample.com');
        $ticket->setDepartureStation('Ternopil');
        $ticket->setArrivalStation('Kyiv');
        $ticket->setDepartureDatetime((new \DateTime('now'))->modify('+2 hours'));
        $ticket->setArrivalDatetime((new \DateTime('now'))->modify('+8 hours'));


        $this->expectException(ValidatorException::class);
        $this->expectExceptionMessage('This value is not a valid email address.');

        $ticket = $this->ticketWorkflow->create($ticket);

        $this->assertNull($ticket->getId());
    }
}
